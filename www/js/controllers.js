angular.module('starter.controllers', [])

.controller('MoedasCtrl', function($window, $http, $scope, $filter) {
  $scope.moedas = JSON.parse($window.localStorage['moedas'] || '[]');
  $scope.favoritos = JSON.parse($window.localStorage['favoritos_moedas'] || '[]');
  $scope.data_dados = $window.localStorage['data_dados_moedas'] || null;
  if($scope.data_dados == null) {
    $scope.data_dados = $filter('date')(new Date(), 'dd/MM/yyyy - HH:mm:ss');
    $window.localStorage['data_dados_moedas'] = $scope.data_dados;
  }
  $scope.listaFavoritos = [];
  
  var popula_lista_favoritos = function() {
    $scope.listaFavoritos = [];
    for(var i=0; i<$scope.moedas.length;i++) {
      if($scope.favoritos.indexOf($scope.moedas[i].codigo) > -1) {
          $scope.listaFavoritos.push($scope.moedas[i]);
      }
    }
  }
  popula_lista_favoritos();
  document.getElementById('data_dados').style.display = 'none';
  document.getElementById('data_dados_atualizando').style.display = 'block';
  $http.get('http://172.18.100.68:8000/api/moedas/').
    success(function(data, status, headers, config) {
      $window.localStorage['moedas'] = JSON.stringify(data);
      $window.localStorage['data_dados_moedas'] = JSON.stringify(new Date());
      $scope.moedas = JSON.parse($window.localStorage['moedas']);
      popula_lista_favoritos();
      $scope.data_dados = $filter('date')(new Date(), 'dd/MM/yyyy - HH:mm:ss');
      $window.localStorage['data_dados_moedas'] = $scope.data_dados;
      document.getElementById('data_dados_atualizando').style.display = 'none';
      document.getElementById('data_dados').style.display = 'block';
  }).
    error(function(error){
      document.getElementById('data_dados_atualizando').style.display = 'none';
      document.getElementById('data_dados').style.display = 'block';
      $scope.data_dados = $scope.data_dados + ' - erro ao atualizar';
  });

  $scope.addFav = function(codigo){
    $scope.favoritos.push(codigo);
    $scope.listaFavoritos = [];
    for(var i=0; i<$scope.moedas.length;i++) {
        if($scope.favoritos.indexOf($scope.moedas[i].codigo) > -1) {
            $scope.listaFavoritos.push($scope.moedas[i]);
        }
    }
    localStorage.setItem('favoritos_moedas', JSON.stringify($scope.favoritos));
  }

  $scope.delFav = function(codigo){
    var newFav = [];
    var toRm;
    for(i=0;i<$scope.listaFavoritos.length;i++){
      if($scope.listaFavoritos[i].codigo != codigo){
        newFav.push($scope.listaFavoritos[i]);
      }
    }
    $scope.listaFavoritos = newFav;
    $scope.favoritos = [];
    for(i=0;i<$scope.listaFavoritos.length;i++){
      $scope.favoritos.push($scope.listaFavoritos[i].codigo);
    }
    if(toRm != undefined){
      $scope.listaFavoritos.pop(i);
    }
    localStorage.setItem('favoritos_moedas', JSON.stringify($scope.favoritos));
  }

  $scope.isFav = function(codigo){
    for(i=0;i<$scope.favoritos.length;i++){
      if($scope.favoritos[i] == codigo) {
        return true;
      }
    }
    return false;
  }

  $scope.isNotFav = function(codigo){
    for(i=0;i<$scope.favoritos.length;i++){
      if($scope.favoritos[i] == codigo) {
        return false;
      }
    }
    return true;
  }
})

.controller('NoticiasCtrl', function($scope, $http, $window, $filter) {
  

  var get_noticias = function ($scope) {
    $http.get('http://172.18.100.68:8000/api/noticias/').
      success(function(data, status, headers, config) {
        $scope.noticias = data;
        $window.localStorage['noticias'] = JSON.stringify(data);
        $scope.data_dados = $filter('date')(new Date(), 'dd/MM/yyyy - HH:mm:ss');
        $window.localStorage['data_dados_noticias'] = JSON.stringify($scope.data_dados);
      }).
        error(function(error){
          alert('problema nas noticias');
      });  
  }

  $scope.noticias = JSON.parse($window.localStorage['noticias'] || '[]');
  if($scope.noticias.length == 0) {
    get_noticias($scope);
  }
  else {
    $scope.data_dados = JSON.parse($window.localStorage['data_dados_noticias']);
  }

  $scope.atualizar = function() {
    get_noticias($scope);
    $scope.$broadcast('scroll.refreshComplete');
    $scope.$apply();
  }

  $scope.get_data = function(date) {
    return new Date(Date.parse('2014-12-23T00:00:00-03:00'))
  }

  $scope.formatar_data = function(date) {
    var date = $scope.get_data(date)
    return date.getDay() + '/' + date.getMonth() + '/' + date.getFullYear();
  }
})

.controller('NoticiaDetailCtrl', function($scope, $stateParams, $window) {
  $scope.noticias = JSON.parse($window.localStorage['noticias'] || '[]');
  for(i=0;i<$scope.noticias.length;i++){
      if($scope.noticias[i].id == $stateParams.noticiaId) {
        $scope.noticia = $scope.noticias[i];
        break;
      }
  }

  $scope.get_data = function(date) {
    return new Date(Date.parse('2014-12-23T00:00:00-03:00'))
  }

  $scope.formatar_data = function(date) {
    var date = $scope.get_data(date)
    return date.getDay() + '/' + date.getMonth() + '/' + date.getFullYear();
  }
})

.controller('EmpresasCtrl', function($scope, Empresas) {
  $scope.empresas = Empresas.all();
})

.controller('EmpresaDetailCtrl', function($scope, $stateParams, Empresas) {
  $scope.empresa = Empresas.get($stateParams.empresaId);

  $scope.carrega_mapa = function() {
    var myLatlng = new google.maps.LatLng(37.3000, -120.4833);
    var mapOptions = {
        center: myLatlng,
        zoom: 16,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    
    var map = new google.maps.Map(document.getElementById("map"), mapOptions);
    document.getElementById("map").style.width = '300px';
    document.getElementById("map").style.height = '300px';
  }

  $scope.carrega_mapa();
})

.controller('EmpresaVagasEmpregoCtrl', function($scope, $stateParams, Empresas) {
  $scope.empresa = Empresas.get($stateParams.empresaId);
  $scope.vagas = $scope.empresa.vagas;
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
  };
});
