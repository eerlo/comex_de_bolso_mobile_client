angular.module('starter.services', [])

.factory('Noticias', function() {

  var noticias = [
      {
        id:4,
        nome: 'Mdic',
        logo: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAATCAIAAAABJ4pRAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABINJREFUeNqUVFtsFFUYPrfdmdnLbLe021baQqWUtdJKL5QiASPiNVqC0TYqUWLShJQmRH3RqPHFRKJvQqIPQNSAAcMDptGEiKjYpLZIkSqGXqjdFlppd3vb7c7Ozpxz/M+2wZD44snszOzJnP//vv///g9XnNiXtNISI1gYq4eUEu4CC7SyiLpJgtSOIJKg/1ySB3x+NmsvJLNpgVUwlAu08iIkzi2pUhHIAa/q1L9p7l6YWmmHEOSBWJD6LixwJ7DDAKR06c6imQOVYxoRyEV4BR3JBYfvqNqBtILrSGNEKgoCsVxWDMmxgAOADeDjQsP+oHbo1egk7B+Mju/vq7kwVYhohsAxoiogiIM5hBaSUIBFfS21WRcOSwUHOwgTqoJR+O0pv31u56XmUKrvt4o/RyNrjVRHw40iLdU9Hck4DPBQxOQKK/XQqQa7nGNb8UICSUoE4y4rCyweqh164b7Jk12N737x2NjtfMAeCDit268e7jj7yOqfX++p+ebvQk5coAB8BQIMroqaf7xt3p4TUATgxj0Y0wOVN96uHomE3I+Ob37zSJvUHURshjySIZ4JPLhp+IcPj3h1eSZ2zxu/R8cXDeThuSLJkGZSY/fGjGOrenG9xlz8vPnK/g0xEwprl52PH9ryUGRLk1lfXWqGCcE0lUqOjxeHVy01141V+5Mvlk+lOLmcAOwC6OpMo8FnqlOCh2j2taqRE1sHKk2LuFJq6NveumNd9f0Dt2IjC1aGVKwzdz1cUryaxSasW9Oo/dFfoaEB6jxdOl2Xt3BtwYw7Xh/VmQNNcNHj5fH3N40CQgnNAAUQMjgh+y/dpIblN9j14fHuHqEFvPUP5L3Svp4mHNtGmgbaVF1oWZNIi8GXextALUpEhLqnJ0o3f9fcEw9iD3RFCaUiMmvmG/lhI50VBQX6ujX5ju3+dHHm1Mmb9dGsxyRKSwylHNLRt2HvL42O4C5IhsLsgEII74+Hd1zY+s5AdMklguMnmkaLwn9RGgr7dSfLk5ZreGhJUdD0+oz0ZxSOMNQ1VdB4fscnw5VCiOUBpb7d91vcklBJJpDwXpwOn50qKdXs2tLZpoqJU99XxVPBbMayXRnOK/Dp5p5tZzpbu0cT+sErG9/6o2rW9iLKQbOget2jQbgay3UIJrlhlJSK6Yz3dKx4dD74Ut3Ivu2X5xe8CSvopdm1kdh7e7/sfO7Ho4PlrT0NvTMhTISaaRhUoImITr244Njz8WwSVKOMhBI1hdIBMQuXlARShxuvPbt+3k0gx9GMsDVp0c7emq8nSiSFjzkVlCuKfHl+Q0aQai21Gb4E7SA59wDWGLLBg+Kk6/lqrKw/4dtWOLfKn/74+r2t3fVX5/IwcyExIBAKFFWOhskyWVx4tG3GmUfKCFRbOHYBNjiC6hBkBTrQRl+mSLeHZkOYOXeMRxIFCb6SME5gYJiGvH7MPn0q5wBCgqkgF4xBEVfdhksTyMmNpFT/chJQJQbkgitE8o73gRe4HsRYe/RJy86IFYtU1pVzSqxC/J8F5fMz/z8CDAAkCSSg0qSX5AAAAABJRU5ErkJggg==',
        site: 'http://www.desenvolvimento.gov.br/portalmdic/sitio/interna/index.php?area=5',
        titulo: 'MDIC divulgará balança comercial de 2014 no dia 5 de janeiro',
        data: '2014-12-23T00:00:00-03:00',
        link: 'http://www.desenvolvimento.gov.br/portalmdic/sitio/interna/noticia.php?area=5&noticia=13552',
        descricao: '<div align="justify"><strong>Bras&iacute;lia (23 de dezembro)</strong> - O Minist&eacute;rio do Desenvolvimento, Ind&uacute;stria e Com&eacute;rcio Exterior (MDIC) voltar&aacute; a divulgar os resultados da balan&ccedil;a comercial de 2014 no dia 5 de janeiro de 2015. Em raz&atilde;o de reforma no edif&iacute;cio sede do MDIC, na Esplanada dos Minist&eacute;rios, o an&uacute;ncio ser&aacute; realizado no audit&oacute;rio do edif&iacute;cio da Secretaria de Com&eacute;rcio Exterior (Secex), localizado na EQN 102/103, lote 1, Asa Norte. </div><div align="justify">&nbsp;</div><div align="justify">&Agrave;s 15h, as informa&ccedil;&otilde;es ser&atilde;o distribu&iacute;das aos jornalistas presentes no audit&oacute;rio da Secex e, &agrave;s 15h10, estar&atilde;o dispon&iacute;veis na p&aacute;gina do MDIC. &Agrave;s 15h30, haver&aacute; entrevista coletiva no local para comentar os resultados. Portanto, n&atilde;o haver&aacute; divulga&ccedil;&atilde;o dos dados semanais no dia 29.<a href="https://www.google.com.br/maps/place/Secex/@-15.780517,-47.882232,17z/data=!3m1!4b1!4m6!1m3!3m2!1s0x0000000000000000:0xa6ec2e65e744d34c!2sSecex!3m1!1s0x0000000000000000:0xa6ec2e65e744d34c" target="_blank" class="linkexterno"></a></div><div align="justify">&nbsp;</div><div align="justify"><a href="https://www.google.com.br/maps/place/Secex/@-15.780517,-47.882232,17z/data=!3m1!4b1!4m6!1m3!3m2!1s0x0000000000000000:0xa6ec2e65e744d34c!2sSecex!3m1!1s0x0000000000000000:0xa6ec2e65e744d34c" target="_blank" class="linkexterno"><strong>Veja como chegar &agrave; Secex.</strong></a><br /></div><br /><p style="text-align: justify"><strong>Mais informa&ccedil;&otilde;es para a imprensa: <br /></strong>Assessoria de Comunica&ccedil;&atilde;o Social do MDIC<br />(61) 2027-7190 e 2027-7198<br /><a href="mailto:ascom@mdic.gov.br">ascom@mdic.gov.br</a></p><p style="text-align: justify"><strong>Redes Sociais:<br /></strong><a href="http://www.twitter.com/mdicgovbr"><strong>www.twitter.com/mdicgov</strong></a><strong> <br /></strong><a href="http://www.facebook.com/mdic.gov"><strong>www.facebook.com/mdic.gov</strong></a><strong> <br /></strong><a href="http://www.youtube.com/user/MdicGovBr"><strong>www.youtube.com/user/MdicGovBr</strong></a></p>'
      },
      {
        id:5,
        nome: 'Mdic',
        logo: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAATCAIAAAABJ4pRAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABINJREFUeNqUVFtsFFUYPrfdmdnLbLe021baQqWUtdJKL5QiASPiNVqC0TYqUWLShJQmRH3RqPHFRKJvQqIPQNSAAcMDptGEiKjYpLZIkSqGXqjdFlppd3vb7c7Ozpxz/M+2wZD44snszOzJnP//vv///g9XnNiXtNISI1gYq4eUEu4CC7SyiLpJgtSOIJKg/1ySB3x+NmsvJLNpgVUwlAu08iIkzi2pUhHIAa/q1L9p7l6YWmmHEOSBWJD6LixwJ7DDAKR06c6imQOVYxoRyEV4BR3JBYfvqNqBtILrSGNEKgoCsVxWDMmxgAOADeDjQsP+oHbo1egk7B+Mju/vq7kwVYhohsAxoiogiIM5hBaSUIBFfS21WRcOSwUHOwgTqoJR+O0pv31u56XmUKrvt4o/RyNrjVRHw40iLdU9Hck4DPBQxOQKK/XQqQa7nGNb8UICSUoE4y4rCyweqh164b7Jk12N737x2NjtfMAeCDit268e7jj7yOqfX++p+ebvQk5coAB8BQIMroqaf7xt3p4TUATgxj0Y0wOVN96uHomE3I+Ob37zSJvUHURshjySIZ4JPLhp+IcPj3h1eSZ2zxu/R8cXDeThuSLJkGZSY/fGjGOrenG9xlz8vPnK/g0xEwprl52PH9ryUGRLk1lfXWqGCcE0lUqOjxeHVy01141V+5Mvlk+lOLmcAOwC6OpMo8FnqlOCh2j2taqRE1sHKk2LuFJq6NveumNd9f0Dt2IjC1aGVKwzdz1cUryaxSasW9Oo/dFfoaEB6jxdOl2Xt3BtwYw7Xh/VmQNNcNHj5fH3N40CQgnNAAUQMjgh+y/dpIblN9j14fHuHqEFvPUP5L3Svp4mHNtGmgbaVF1oWZNIi8GXextALUpEhLqnJ0o3f9fcEw9iD3RFCaUiMmvmG/lhI50VBQX6ujX5ju3+dHHm1Mmb9dGsxyRKSwylHNLRt2HvL42O4C5IhsLsgEII74+Hd1zY+s5AdMklguMnmkaLwn9RGgr7dSfLk5ZreGhJUdD0+oz0ZxSOMNQ1VdB4fscnw5VCiOUBpb7d91vcklBJJpDwXpwOn50qKdXs2tLZpoqJU99XxVPBbMayXRnOK/Dp5p5tZzpbu0cT+sErG9/6o2rW9iLKQbOget2jQbgay3UIJrlhlJSK6Yz3dKx4dD74Ut3Ivu2X5xe8CSvopdm1kdh7e7/sfO7Ho4PlrT0NvTMhTISaaRhUoImITr244Njz8WwSVKOMhBI1hdIBMQuXlARShxuvPbt+3k0gx9GMsDVp0c7emq8nSiSFjzkVlCuKfHl+Q0aQai21Gb4E7SA59wDWGLLBg+Kk6/lqrKw/4dtWOLfKn/74+r2t3fVX5/IwcyExIBAKFFWOhskyWVx4tG3GmUfKCFRbOHYBNjiC6hBkBTrQRl+mSLeHZkOYOXeMRxIFCb6SME5gYJiGvH7MPn0q5wBCgqkgF4xBEVfdhksTyMmNpFT/chJQJQbkgitE8o73gRe4HsRYe/RJy86IFYtU1pVzSqxC/J8F5fMz/z8CDAAkCSSg0qSX5AAAAABJRU5ErkJggg==',
        site: 'http://www.desenvolvimento.gov.br/portalmdic/sitio/interna/index.php?area=5',
        titulo: 'Balança comercial da terceira semana de dezembro tem exportações de US$ 4,131 bilhões',
        data: '2014-12-22T00:00:00-03:00',
        link: 'http://www.desenvolvimento.gov.br/portalmdic/sitio/interna/noticia.php?area=5&noticia=13551',
        descricao: '<div align="justify"><strong>Bras&iacute;lia (22 de dezembro) - </strong>A balan&ccedil;a comercial da terceira semana de dezembro fechou com exporta&ccedil;&otilde;es de US$ 4,131 bilh&otilde;es (m&eacute;dia di&aacute;ria de US$ 826,2 milh&otilde;es) e importa&ccedil;&otilde;es de US$ 5,470 bilh&otilde;es (m&eacute;dia di&aacute;ria de US$ 1,094 bilh&atilde;o). Como resultado, o saldo comercial (diferen&ccedil;a entre exporta&ccedil;&otilde;es e importa&ccedil;&otilde;es) foi deficit&aacute;rio em US$ 1,339 bilh&atilde;o (m&eacute;dia di&aacute;ria negativa de US$ 267,8 milh&otilde;es). J&aacute; a corrente de com&eacute;rcio - soma das duas opera&ccedil;&otilde;es - foi de US$ 9,601 bilh&otilde;es (m&eacute;dia di&aacute;ria de US$ 1,920 bilh&atilde;o).</div><p align="justify">A m&eacute;dia das exporta&ccedil;&otilde;es da terceira semana foi 3,8% inferior &agrave; m&eacute;dia de US$ 858,8 milh&otilde;es registrada nas duas primeiras semanas do m&ecirc;s. O motivo foi a retra&ccedil;&atilde;o nas exporta&ccedil;&otilde;es de produtos semimanufaturados (-14,9%) - a&ccedil;&uacute;car em bruto, celulose, couros e peles, ferro-ligas e ferro fundido; e b&aacute;sicos (-13,4%) - petr&oacute;leo em bruto, carne de frango, bovina e su&iacute;na, milho em gr&atilde;o, fumo em folhas e soja em gr&atilde;o; enquanto cresceram as vendas de manufaturados (13,0%) - avi&otilde;es, &oacute;leos combust&iacute;veis, laminados planos, autom&oacute;veis de passageiros e m&aacute;quinas para terraplanagem. Do lado das importa&ccedil;&otilde;es, houve crescimento de 40,1%, sobre igual per&iacute;odo comparativo - m&eacute;dia da terceira semana (US$ 781,0 milh&otilde;es) frente &agrave; m&eacute;dia at&eacute; a segunda semana (US$ 1,094 bilh&atilde;o), explicado, principalmente, pelo aumento nos gastos com combust&iacute;veis e lubrificantes.</p><p><strong>M&ecirc;s</strong></p><p align="justify">Nas tr&ecirc;s primeiras semanas de dezembro, com 15 dias &uacute;teis, as exporta&ccedil;&otilde;es foram de US$ 12,719 bilh&otilde;es (m&eacute;dia di&aacute;ria de US$ 847,9 milh&otilde;es) e as importa&ccedil;&otilde;es de US$ 13,280 bilh&otilde;es (m&eacute;dia di&aacute;ria de US$ 885,3 milh&otilde;es). O saldo comercial do per&iacute;odo ficou negativo em US$ 561 milh&otilde;es (m&eacute;dia di&aacute;ria de menos US$ 37,4 milh&otilde;es), enquanto a corrente de com&eacute;rcio totalizou US$ 25,999 bilh&otilde;es (m&eacute;dia di&aacute;ria de US$ 1,733 bilh&atilde;o).</p><p align="justify">Nas exporta&ccedil;&otilde;es, comparadas as m&eacute;dias at&eacute; a terceira semana de dezembro de 2014 (US$ 847,9 milh&otilde;es) com a de dezembro de 2013 (US$ 992,7 milh&otilde;es), houve queda de 14,6% em raz&atilde;o da diminui&ccedil;&atilde;o nas vendas de produtos manufaturados (-20,8%) - &oacute;leos combust&iacute;veis, autom&oacute;veis de passageiros, motores e geradores, ve&iacute;culos de carga, avi&otilde;es e motores para ve&iacute;culos; semimanufaturados (-11,2%) - alum&iacute;nio em bruto, &oacute;leo de soja em bruto, ferro-ligas, celulose, a&ccedil;&uacute;car em bruto e ouro em forma semimanufaturada; e b&aacute;sicos (-10,1%) - min&eacute;rio de ferro, farelo de soja, min&eacute;rio de cobre e carne bovina. Em rela&ccedil;&atilde;o a novembro de 2014, o crescimento nas compras externas foi de 8,4% - produtos manufaturados (11,9%) e b&aacute;sicos (10,1%) -, enquanto retrocederam as vendas de semimanufaturados (-0,5%).</p><p align="justify">Nas importa&ccedil;&otilde;es, a m&eacute;dia di&aacute;ria das tr&ecirc;s primeiras semanas de dezembro de 2014 ficou 2,2% acima da m&eacute;dia de dezembro de 2013 (US$ 866,5 milh&otilde;es). Nesse comparativo, cresceram as compras com combust&iacute;veis e lubrificantes (31,3%), pl&aacute;sticos e obras (8,3%), qu&iacute;micos org&acirc;nicos/inorg&acirc;nicos (7,4%), farmac&ecirc;uticos (7,4%), instrumentos de &oacute;tica e precis&atilde;o (5%) e equipamentos eletroeletr&ocirc;nicos (4,7%). Ante novembro de 2014, houve retra&ccedil;&atilde;o de 1,6% nas compras externas - adubos e fertilizantes (-38,8%), equipamentos eletroeletr&ocirc;nicos (-15,6%), sider&uacute;rgicos (-11,8%), equipamentos mec&acirc;nicos (-7,1%), ve&iacute;culos autom&oacute;veis e partes (-5,9%) e qu&iacute;micos org&acirc;nicos/inorg&acirc;nicos (-4,7%).</p><p><strong>Ano</strong></p><p align="justify">No acumulado do ano, com 246 dias &uacute;teis, as exporta&ccedil;&otilde;es totalizam US$ 220,329 bilh&otilde;es (m&eacute;dia di&aacute;ria de US$ 895,6 milh&otilde;es) e as importa&ccedil;&otilde;es, US$ 225,113 bilh&otilde;es (m&eacute;dia di&aacute;ria de US$ 915,1 milh&otilde;es). Pela m&eacute;dia di&aacute;ria, em rela&ccedil;&atilde;o ao mesmo per&iacute;odo do ano passado, diminu&iacute;ram as exporta&ccedil;&otilde;es (- 6,6%) e as importa&ccedil;&otilde;es (- 4,2%). </p><p align="justify">A corrente de com&eacute;rcio deste ano est&aacute; em US$ 445,442 bilh&otilde;es (m&eacute;dia di&aacute;ria de US$ 1,810 bilh&atilde;o) e o saldo comercial mant&eacute;m d&eacute;ficit de US$ 4,784 bilh&otilde;es (m&eacute;dia di&aacute;ria de menos US$ 19,4 milh&otilde;es). Considerando o resultado m&eacute;dio por dia &uacute;til, a corrente de com&eacute;rcio deste ano est&aacute; 5,4% menor que a verificada no mesmo per&iacute;odo de 2013.</p><p><a href="http://www.mdic.gov.br/sitio/interna/interna.php?area=5&amp;menu=567" target="_blank">Acesse as informa&ccedil;&otilde;es da balan&ccedil;a comercial no per&iacute;odo.</a></p><p><strong><br />Mais informa&ccedil;&otilde;es para a imprensa:</strong><br />Assessoria de Comunica&ccedil;&atilde;o Social do MDIC<br />(61) 2027-7190 e 2027-7198<br />ascom@mdic.gov.br<br /> <strong><br />Redes Sociais:</strong><br />www.twitter.com/mdicgov <br />www.facebook.com/mdic.gov<br />www.youtube.com/user/MdicGovBr</p>'
      }
  ];

  return {
    all: function() {
      return noticias;
    },
    get: function(noticiaId) {
      for(var i=0;i<noticias.length;i++){
        if(noticias[i].id == noticiaId){
          return noticias[i];
        }
      }
    }
  }
})

/**
 * A simple example service that returns some data.
 */
.factory('Empresas', function() {
  
  var empresas = [{
    id: 0,
    nome: 'AGE Manager',
    descricao: 'Enjoys drawing things',
    logo: 'img/age.png',
    localizacao: 'Caxias do Sul',
    endereco: 'Av. Itália, Bairro São Pelegrino',
    cidade: 'Caxias do Sul - RS',
    telefone: '9198-1287',
    email: 'eduardo.erlo@gmail.com',
    ramo: 'Consultoria Especializada',
    vagas: [{nome: 'Analista de Importação', descricao: 'A pessoa deve ser muito legal. Salário: R$ 3mil reais.'}]
  },
  {
    id: 1,
    nome: 'Efficienza Importação e Exportação',
    descricao: 'Empresa destinada a consultoria de importação e exportação',
    logo: 'img/efficienza.png',
    localizacao: 'Caxias do Sul',
    cidade: 'Caxias do Sul - RS',
    endereco: 'Rua vinte de setembro, bairro Centro',
    telefone: '9198-1287',
    email: 'contato_efficienza@gmail.com',
    ramo: 'Despachante Aduaneiro',
    vagas: []
  }
  ];


  return {
    all: function() {
      return empresas;
    },
    get: function(empresaId) {
      for(var i=0;i<empresas.length;i++){
        if(empresas[i].id == empresaId){
          return empresas[i];
        }
      }
    }
  }
})

.factory('Moedas', function() {
  
  var moedas = [{
    codigo: 'BRL',
    taxa_compra: 1.72,
    taxa_venda: 10.15
  },
  {
    codigo: 'USD',
    taxa_compra: 1.72,
    taxa_venda: 10.15
  },
  {
    codigo: 'ABC',
    taxa_compra: 1.72,
    taxa_venda: 10.15
  },
  {
    codigo: 'JPY',
    taxa_compra: 1.72,
    taxa_venda: 10.15
  },
  {
    codigo: 'ADD',
    taxa_compra: 1.72,
    taxa_venda: 10.15
  }];


  return {
    all: function() {
      return moedas;
    }
  }
});
